#include <iostream>
#include <cstring>



using namespace std;

//Variaveis Globais

//Protótipo da Função


//Programa Principal
int main(void)
{
    struct funcionario{
        char    nome[64];
        long    id_funcionario;
        float   salario;
        char    telefone[16];
        int     numero_sala;
    }trabalhador1, trabalhador2;

    //Copia um nome para uma string
    std::strcpy(trabalhador1.nome, "Primeiro Nome");

    trabalhador1.id_funcionario = 1001;;
    trabalhador1.salario = 2500.00;
    trabalhador1.numero_sala = 102;

    //copia um numero de telefone para a string
    std::strcpy(trabalhador1.telefone, "555-1234");


        //Copia um nome para uma string
    std::strcpy(trabalhador2.nome, "Segundo Nome");

    trabalhador2.id_funcionario = 1002;;
    trabalhador2.salario = 2500.00;
    trabalhador2.numero_sala = 103;

    //copia um numero de telefone para a string
    std::strcpy(trabalhador2.telefone, "555-4321");

    cout << "Funcionario: " << trabalhador1.nome << endl;
    cout << "Telefone: " << trabalhador1.telefone << endl;
    cout << "Id. do Funcionario: " << trabalhador1.id_funcionario << endl;
    cout << "Salario: " << trabalhador1.salario << endl;
    cout << "Sala: " << trabalhador1.numero_sala << endl;

    cout << "Funcionario: " << trabalhador2.nome << endl;
    cout << "Telefone: " << trabalhador2.telefone << endl;
    cout << "Id. do Funcionario: " << trabalhador2.id_funcionario << endl;
    cout << "Salario: " << trabalhador2.salario << endl;
    cout << "Sala: " << trabalhador2.numero_sala << endl;

    return(0);
}
